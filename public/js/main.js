document.addEventListener('DOMContentLoaded', () => {
    main();
});

function main() {
    const productData = [ // объект с данными о  товарах 
        {
            title: 'Суши',
            price: 1000,
        }, {
            title: 'Мороженое',
            options: ['Шоколадная крошка', 'Орехи', 'Сироп'],
            price: 100,
            optionsPrice: 20,

        }, {
            title: 'Пицца',
            property: 'Сердечко на коробке',
            price: 700,
            propertyPrice: 100,
        }
    ];

    document.getElementById('select').addEventListener('change', showDisplay(productData));
}

const showDisplay = (productData) => { // вывод дисплея на экран
    return function f() {
        const customization = document.querySelector('.customization');

        selectedOption = this.selectedOptions[0].id;//id выбранного option
        product = createDisplay(productData[selectedOption]);//создание дисплея

        customization.appendChild(product);
    }
};

function createDisplay(product) { //создание дисплея для выбранного товара
    resetDisplay();
    const outputSpan = document.getElementById('output');
    outputSpan.innerHTML = "";

    const item = document.createElement('div');// основной div

    const kolvoInput = document.createElement('input');
    kolvoInput.setAttribute('id', 'kolvo');
    kolvoInput.placeholder = 'введите кол-во';
    item.appendChild(kolvoInput);

    //создание опций товара
    if (product.options) {
        for (let i = 0; i < product.options.length; i++) {
            const optionsLabel = document.createElement('label');
            const optionsInput = document.createElement('input');
            optionsInput.setAttribute('id', 'options');
            optionsInput.type = 'radio';

            optionsLabel.innerHTML = product.options[i];
            optionsLabel.appendChild(optionsInput);
            item.appendChild(optionsLabel);
        }
    } else {
        const p = document.createElement('p');

        p.innerHTML = 'У товара нет  опций';
        item.appendChild(p);
    }

    //создание свойства товара
    if (product.property) {
        const propertiesLabel = document.createElement('label');

        const propertiesInput = document.createElement('input');
        propertiesInput.setAttribute('id', 'property');
        propertiesInput.type = 'checkbox';

        propertiesLabel.innerHTML = product.property;
        propertiesLabel.appendChild(propertiesInput);

        item.appendChild(propertiesLabel);
    } else {
        const p = document.createElement('p');

        p.innerHTML = 'У товара нет  свойств';
        item.appendChild(p);
    }

    //создание кнопки для подсчёта цены
    const button = document.createElement('button');
    button.innerHTML = 'Рассчитать заказ';
    button.setAttribute('id', 'submit');
    item.appendChild(button);

    item.className = 'item';

    button.addEventListener('click', calcOrderPrice(product));

    return item;
}

//обновление дисплея
function resetDisplay() {
    item = document.querySelector('.item');
    if (item) item.remove();
}

const calcOrderPrice = (product) => { //калькулятор для цены
    return function f() {
        const outputSpan = document.getElementById('output');
        const kolvo = document.getElementById('kolvo').value;
        const options = document.querySelectorAll('#options');
        const property = document.getElementById('property');
        
        if (kolvo.match(/[0-9]/i) === null) {
            alert('Введите корректные данные');
        }
        else {
            let optionsPrice = product.price * kolvo;

            

            for (let i = 0; i < options.length; i++) {
                if (options && options[i].checked) {
                    optionsPrice = optionsPrice + product.optionsPrice;//цена товара + цена опции
                }
            }
            if (property && property.checked)
                optionsPrice = optionsPrice + product.propertyPrice;//цена товара + цена свойства

            outputSpan.innerHTML = "Цена заказа = " + optionsPrice;
        }
    }
};
